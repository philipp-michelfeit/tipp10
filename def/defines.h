/*
Copyright (c) 2006-2011, Tom Thielicke IT Solutions

SPDX-License-Identifier: GPL-2.0-only
*/

/****************************************************************
**
** File name: defines.h
**
****************************************************************/

#ifndef DEFINES_H
#define DEFINES_H

#if APP_PORTABLE || defined(APP_WIN)
#include <QCoreApplication>
#endif
#include <QObject>
#include <QString>
#include <QApplication>

namespace t10 {

inline QString getDataDir()
{
#if APP_PORTABLE || defined(APP_WIN)
    return QCoreApplication::applicationDirPath() + "/../share/tipp10/";
#else

#ifndef INSTALLPREFIX
#define INSTALLPREFIX "/usr/local"
#endif
    // must end with a trailing /
    return QString(INSTALLPREFIX "/share/tipp10/");
#endif
}

// Languages
const QString app_std_language_layout {"de_qwertz_win"};
const QString app_std_language_lesson {"de_de_qwertz"};

// Common program constants
const QString app_name_intern {"TIPP10"};
const QString app_name {"TIPP10"};
const QString app_url {"https://gitlab.com/philipp-michelfeit/tipp10"};
const QString app_db {"tipp10v2.template"};
const QString app_user_db {"tipp10v2.db"};
const QString app_version {"3.10.0"};
const QString qt_version {QT_VERSION_STR};

// Update constants
const QString update_url_sql {"/update/sql.tipp10v210.utf"};
const auto compiled_update_version {33};

// Lesson text constants
const auto num_token_until_refresh {25};
const auto num_token_until_new_line {35};
const auto num_intelligent_queries {2};
const QChar token_new_line {QChar(0x00b6)};
const QChar token_tab {QChar(0x2192)};
const QChar token_backspace {QChar(0x00b9)}; // 0x00ac

// Constants for dynamic training
const auto num_text_until_repeat {10};
const auto border_lesson_is_sentence {7};
const auto last_lession {18}; // lesson with training of all characters
const auto numpad_lesson_start {19};
const auto synchron_db_while_training {false};

// Standard constants
const auto lesson_timelen_standard {5};
const auto lesson_tokenlen_standard {500};
const auto metronom_standard {60};
const auto tickerspeed_standard {2};
const QString ticker_color_font {"#000000"};
const QString ticker_color_bg {"#FFFFFF"};
const QString ticker_color_cursor {"#CDCDCD"};

// Font format
const QString font_standard {"Arial"};
const auto font_size_ticker {18};
const auto font_size_ticker_pause {16};
const auto font_size_status {8};
const auto font_size_progress {8};
const auto font_size_progress_lesson {6};
const auto font_size_finger {10};

// Window dimensions
const auto app_width_standard {750};
const auto app_height_standard {520};
const auto app_width_small {680};
const auto app_height_small {210};
}

#endif // DEFINES_H
