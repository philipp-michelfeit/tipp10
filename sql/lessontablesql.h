/*
Copyright (c) 2006-2009, Tom Thielicke IT Solutions

SPDX-License-Identifier: GPL-2.0-only
*/

/****************************************************************
**
** Definition of the LessonTableSql class
** File name: lessontablesql.h
**
****************************************************************/

#include <QChar>
#include <QComboBox>
#include <QHeaderView>
#include <QLabel>
#include <QList>
#include <QModelIndex>
#include <QPushButton>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QTableView>
#include <QVariant>
#include <QWidget>

#ifndef LESSONSQLMODEL_H
#define LESSONSQLMODEL_H

//! The LessonSqlModel class provides a table model to format cells.
/*!
        @author Tom Thielicke, s712715
        @version 0.0.7
        @date 21.06.2006
*/

class LessonSqlModel : public QSqlQueryModel {
    Q_OBJECT

public:
    LessonSqlModel(int row, int type, QWidget* parent = 0);
    int lastIdInserted;

private:
    int lastTypeInserted;
    QVariant data(const QModelIndex& index, int role) const;
    QWidget* parentWidget;
    QString language;
};

#endif // LESSONSQLMODEL_H

#ifndef LESSONTABLESQL_H
#define LESSONTABLESQL_H

//! The LessonTableSql class provides a table widget with lessons.
/*!
        @author Tom Thielicke, s712715
        @version 0.0.2
        @date 16.06.2006
*/
class LessonTableSql : public QWidget {
    Q_OBJECT

public:
    LessonTableSql(int row, int type, QList<QChar> charlist,
        QList<int> mistakelist, QWidget* parent = 0);

private slots:
    void sortColumn(int columnindex);
    void changeFilter(int rowindex);

private:
    void setModelHeader();
    QLabel* labelFilter;
    QComboBox* comboFilter;
    LessonSqlModel* model;
    QTableView* view;
    QHeaderView* headerview;
    QVariant data(const QModelIndex& item, int role) const;
    void setQueryOrder(QString columnname, bool isdesc);
    int previousColumnIndex;
    QString columnName;
    QString whereClause;
    bool isDesc;
    QList<QChar> charList;
    QList<int> mistakeList;
    int lessonRow;
};

#endif // LESSONTABLESQL_H
